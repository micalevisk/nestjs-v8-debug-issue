import { Module, DynamicModule } from '@nestjs/common'
import { getRedisConnectionToken, RedisModule } from '@nestjs-modules/ioredis'

const STORE_CONNECTION_NAME = 'BAR'
const MyModule: DynamicModule = {
  module: class {},
  imports: [
    RedisModule.forRootAsync({
        useFactory: () => ({
          config: { 
            url: 'redis://localhost:6379',
          },
        }),
      },
      STORE_CONNECTION_NAME
    )
  ],
  // The following line triggers the error
  providers: [
    {
      // provide: 'Foo', // This works
      provide: Symbol('foo'), // Using Symbol doesn't
      // The following line triggers the error
      inject: [getRedisConnectionToken(STORE_CONNECTION_NAME)],
      useFactory: () => {},
    }
  ]
}

@Module({
  imports: [
    MyModule,
  ],
})
export class AppModule {}
